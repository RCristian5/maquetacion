

var jade = require('gulp-jade');
var gulp = require('gulp');
var stylus = require('gulp-stylus');
var watch = require('gulp-watch');
var server = require('gulp-server-livereload');
var gulpsync = require('gulp-sync')(gulp);


gulp.task('jade', function () {
    gulp.src('source/*.jade')
        .pipe(jade({
          pretty: true
        }))
        .pipe(gulp.dest('build/'))
});

gulp.task('stylus', function () {
  return gulp.src('source/*.styl')
    .pipe(stylus())
    .pipe(gulp.dest('build/'));
});

gulp.task('watch', function () {
    gulp.watch( 'source/*.jade', ['jade']);
    gulp.watch( 'source/*.styl', ['stylus']);
});

gulp.task('webserver', function() {
    gulp.src('build/')
    .pipe(server({
        directoryListing: {
            enable: false,
            path: 'build/'
        },
        livereload: true,
        open: true,
        port: 3000
    }));
});

gulp.task('default', gulpsync.sync([
    // sync
        // async
    [
        'jade',
        'stylus'
    ],
    'watch',
    'webserver'
]));
